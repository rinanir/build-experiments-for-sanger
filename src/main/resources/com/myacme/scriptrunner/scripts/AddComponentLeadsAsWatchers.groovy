package com.acme.scriptrunner.scripts

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue

// this comes to us in the binding... this line is unnecessary other than to give my IDE type information
Issue issue = issue

// get some components we need
def watcherManager = ComponentAccessor.getWatcherManager()
def userUtil = ComponentAccessor.getUserUtil()

// would be better to use componentLead rather than lead, but not available until 6.3
issue.componentObjects*.lead.each {String username ->
    def applicationUser = userUtil.getUserByKey(username)
    if (applicationUser) {
        watcherManager.startWatching(applicationUser, issue)
    }
}
