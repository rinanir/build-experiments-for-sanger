package com.acme.scriptrunner.scripts

import com.atlassian.jira.issue.Issue

/**
 * Custom field script that displayes the earliest release date for any "fix versions"
 * Just an example but might be useful for JQL queries etc... eg to help find issues that really need to be done
 */

Issue issue = issue

def fixVersions = issue.fixVersions
if (fixVersions) {
    def releaseDates = fixVersions.findResults { it.releaseDate }.sort()
    if (releaseDates) {
        return releaseDates.first()
    }
}

