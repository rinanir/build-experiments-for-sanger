package com.myacme.scriptrunner.test

import groovy.util.logging.Log4j
import spock.lang.Specification



@Log4j
class SampleTest extends Specification {

    def "first sample test"() {
        setup:
        log.debug("do setup here")

        when: "do something to something..."
        1

        then: "check the outcomes"
        true
    }


    def "second sample test"() {
        expect:
        true
    }

    /**
     * this shows what happens when a test fails
     */
    def "failing test"() {
        setup:
        log.debug("run failing test")

        when: "do something"
        1

        then: "this test fails"
        // foo()
        1 // change to 0 to make it fail
    }

    def foo() {
        bar()
    }

    def bar() {
        assert false
    }
}
