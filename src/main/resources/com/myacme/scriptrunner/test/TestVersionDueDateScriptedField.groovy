package com.acme.scriptrunner.test

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueInputParametersImpl
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.project.version.Version
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import com.onresolve.scriptrunner.test.AbstractScriptFieldSpecification
import com.onresolve.scriptrunner.test.ScriptFieldCreationInfo
import spock.lang.Shared

class TestVersionDueDateScriptedField extends AbstractScriptFieldSpecification {

    @Shared def versionManager = ComponentAccessor.getVersionManager()
    @Shared Version VER1_0
    @Shared Version VER1_1
    @Shared Version VER1_2
    @Shared CustomField fieldUnderTest

    def setupSpec() {
        // add a few of Versions for this project, with varying release dates
        VER1_0 = versionManager.createVersion("1.0", null, "no release date", testProject.id, null)
        VER1_1 = versionManager.createVersion("1.1", new Date() + 7, "release date one week in future", testProject.id, null)
        VER1_2 = versionManager.createVersion("1.2", new Date() + 14, "release date two weeks in future", testProject.id, null)

        // easy way to create a new scripted custom field, and set its configuration
        def scriptFieldCreation = ScriptFieldCreationInfo.Builder.newBuilder()  
            .setName("Fix Version Due Date")
            .setSearcherKey(ScriptRunnerImpl.PLUGIN_KEY + ":datetimerange")
            .setTemplate("datetime")
            .setTestProjectContext()
            .setScriptFile('com/acme/scriptrunner/scripts/VersionDueDateScriptField.groovy')
            // .setScript("a script here") use this instead of setScriptFile to set up code inline
            .setNote("shows closest fixversion due date")
            .build()
        fieldUnderTest = scriptFieldCreation.create()
    }

    def "test due date script field"() {
        setup:
        def project = getTestProject()

        // create an issue to test the fields with
        def params = new IssueInputParametersImpl()

        params.with {
            setProjectId(project.id)
            setIssueTypeId(getBugIssueType().id)
            setReporterId(user.name)
            setSummary("my summary")
            setFixVersionIds(versions*.id as Long[])
            setAssigneeId(user.name)
        }

        when:
        def createValidationResult = issueService.validateCreate(cwdUser, params)

        then:
        createValidationResult.isValid()

        when:
        def issue = issueService.create(cwdUser, createValidationResult).issue
        assert issue.isCreated()
        log.debug("Create issue ${issue.key}")

        then: "cf value should be null as no versions are associated"
        expectedValue == issue.getCustomFieldValue(fieldUnderTest)

        where:
        versions                    || expectedValue
        []                          || null                 // no fix version, no value
        [VER1_0]                    || null                 // this release has no release date, no value
        [VER1_1]                    || VER1_1.releaseDate   // show ver1 release date
        [VER1_1, VER1_2]            || VER1_1.releaseDate   // show ver1 release date, as before ver2
        [VER1_0, VER1_1, VER1_2]    || VER1_1.releaseDate   // checking same result with a version with no release date
    }
}
