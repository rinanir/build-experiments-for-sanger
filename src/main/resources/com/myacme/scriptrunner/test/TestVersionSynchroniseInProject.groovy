package com.acme.scriptrunner.test

import com.atlassian.jira.bc.JiraServiceContext
import com.atlassian.jira.bc.JiraServiceContextImpl
import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.bc.project.ProjectCreationData
import com.atlassian.jira.bc.project.ProjectService
import com.atlassian.jira.bc.project.version.RemoveVersionAction
import com.atlassian.jira.bc.project.version.SwapVersionAction
import com.atlassian.jira.bc.project.version.VersionBuilderImpl
import com.atlassian.jira.bc.project.version.VersionService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.project.*
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueInputParameters
import com.atlassian.jira.project.AssigneeTypes
import com.atlassian.jira.project.Project
import com.atlassian.jira.project.type.JiraApplicationAdapter
import com.atlassian.jira.project.version.Version
import com.onresolve.scriptrunner.canned.jira.workflow.listeners.VersionSynchroniseListener
import com.onresolve.scriptrunner.runner.ListenerManager
import com.onresolve.scriptrunner.runner.ListenerManagerImpl
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import com.onresolve.scriptrunner.runner.util.OSPropertyPersister
import com.onresolve.scriptrunner.test.AbstractWorkflowSpecification
import spock.lang.Shared

class TestVersionSynchroniseInProject extends AbstractWorkflowSpecification {
    public static final String TEST_SOURCE_PROJECT_KEY = "SRTESTPR1"
    public static final String TEST_TARGET_PROJECT_KEY = "SRTESTPR2"

    @Shared
    String TEST_SRC_PREFIX = "SR_TEST_SRC"

    @Shared
    String TEST_TARGET_PREFIX = "SR_TEST_TARGET"

    @Shared
    def versionManager = ComponentAccessor.getVersionManager()

    @Shared
    def ctx = new JiraServiceContextImpl(user)

    @Shared
    def versionService = ComponentAccessor.getComponent(VersionService)

    public static final listenerManager = ScriptRunnerImpl.getPluginComponent(ListenerManager) as ListenerManager
    public static final String VER_STR1_1 = "1.1"
    public static final String VER_STR_2_2 = "2.2"
    public static final String VER_STR_3_3 = "3.3"

    @Shared
    Version VER1_1
    @Shared
    Project TARGET_PROJECT
    @Shared
    Project SOURCE_PROJECT

    def setup() {
        deleteExistingProject(TEST_SOURCE_PROJECT_KEY)
        deleteExistingProject(TEST_TARGET_PROJECT_KEY)

        //create source project
        def sourceProjectCreationData = new ProjectCreationData.Builder().with {
            withName(TEST_SRC_PREFIX + " Project")
            withKey(TEST_SOURCE_PROJECT_KEY)
            withDescription("source project for testing")
            withLead(user)
            withUrl(null)
            withAssigneeType(AssigneeTypes.PROJECT_LEAD)
            withType(JiraApplicationAdapter.BUSINESS_KEY)
        }.build()

        final ProjectService.CreateProjectValidationResult srcProjectValidationResult = projectService.validateCreateProject(user, sourceProjectCreationData)
        assert !srcProjectValidationResult.errorCollection.hasAnyErrors()
        SOURCE_PROJECT = projectService.createProject(srcProjectValidationResult)

        //create target project
        def targetProjectCreationData = new ProjectCreationData.Builder().with {
            withName(TEST_TARGET_PREFIX + " Project")
            withKey(TEST_TARGET_PROJECT_KEY)
            withDescription("target project for testing")
            withLead(user)
            withUrl(null)
            withAssigneeType(AssigneeTypes.PROJECT_LEAD)
            withType(JiraApplicationAdapter.BUSINESS_KEY)
        }.build()

        final ProjectService.CreateProjectValidationResult targetProjectValidationResult = projectService.validateCreateProject(user, targetProjectCreationData)
        assert !targetProjectValidationResult.errorCollection.hasAnyErrors()
        TARGET_PROJECT = projectService.createProject(targetProjectValidationResult)
    }

    def cleanup() {
        deleteExistingProject(TEST_SOURCE_PROJECT_KEY)
        deleteExistingProject(TEST_TARGET_PROJECT_KEY)
        //this project is created by AbstractWorkflowSpecification. need to delete the project
        deleteExistingProject(TEST_PROJECT_KEY)

        OSPropertyPersister.remove(ListenerManagerImpl.CONFIG_LISTENERS)
    }

    def "test create version if not available"() {
        setup:
        def eventList = [VersionCreateEvent.name]
        createTestListener(eventList)
        //validate new project creation
        def result =  versionService.validateCreateVersion(user, SOURCE_PROJECT, VER_STR1_1, new Date(), "version is set to 1.1",  null)

        when: "a new version is created in source project"
        if(result.isValid()){
            versionService.createVersion(user, result)
        }

        then: "same version should be created in target project"
        Version version = TARGET_PROJECT.versions.find { Version v ->
            v.name == VER_STR1_1
        }
        assert version != null
        assert TARGET_PROJECT.versions.size() == 1
    }

    def "test version synchronise archive"() {
        setup:
        def eventList = [VersionArchiveEvent.name]
        createTestListener(eventList)
        //creating version for testSourceProject and testTargetProject
        VER1_1 = createTestVersion(SOURCE_PROJECT, VER_STR1_1, "version is set to 1.1 for source project")
        createTestVersion(TARGET_PROJECT, VER_STR1_1, "version is set to 1.1 in target project")

        //validating result
        def result = versionService.validateArchiveVersion(user, VER1_1)

        when: "source version is archived"
        if(result.isValid()){
            versionService.archiveVersion(result)

        }

        then: "target version archived flag should be true"
        Version version = TARGET_PROJECT.versions.find { Version v ->
            v.name == VER_STR1_1
        }
        assert version?.isArchived()
    }

    def "test version synchronise release"() {
        setup:
        def eventList = [VersionReleaseEvent.name]
        createTestListener(eventList)
        //creating version for testSourceProject and testTargetProject
        VER1_1 = createTestVersion(SOURCE_PROJECT, VER_STR1_1, "version is set to 1.1 for source project")
        createTestVersion(TARGET_PROJECT, VER_STR1_1, "version is set to 1.1 in target project")
        //validating result
        def result = versionService.validateReleaseVersion(cwdUser, VER1_1, new Date())

        when:
        if(result.isValid()){
            versionService.releaseVersion(result)
        }

        then: "version release flag should be true"
        Version version = TARGET_PROJECT.versions.find { Version v ->
            v.name == VER_STR1_1
        }
        assert version?.isReleased()
    }

    def "test version delete when the version is not referred as affected or fix version "() {
        setup:
        def eventList = [VersionDeleteEvent.name]
        createTestListener(eventList)
        //creating version for testSourceProject and testTargetProject
        VER1_1 = createTestVersion(SOURCE_PROJECT, VER_STR1_1, "version is set to 1.1 for source project")
        final def VER2_2 = createTestVersion(SOURCE_PROJECT, VER_STR_2_2, "version is set to 2.2 for source project")
        createTestVersion(TARGET_PROJECT , VER_STR1_1, "target version is set to 1.1")

        def result = versionService.validateDelete(ctx,VER1_1.id, new SwapVersionAction(VER2_2.id), new RemoveVersionAction() )

        when: "when source version is deleted"
        if(result.isValid()){
            versionService.delete(ctx, result)
        }

        then: "target version should be deleted"
        Version version = TARGET_PROJECT.versions.find { Version v ->
            v.name == VER_STR1_1
        }

        assert TARGET_PROJECT.versions.size() == 0
        assert version == null
    }

    def "test version delete when the version is referred as affected version"() {
        setup:
        def eventList = [VersionDeleteEvent.name]
        createTestListener(eventList)

        //creating version for testSourceProject and testTargetProject
        VER1_1 = createTestVersion(SOURCE_PROJECT, VER_STR1_1, "version is set to 1.1 for source project")
        final def TARGET_VER_1 = createTestVersion(TARGET_PROJECT , VER_STR1_1, "target version is set to 1.1")

        final def VER_STR_2_2 = "2.2"
        def SRC_VER2_2 =createTestVersion(SOURCE_PROJECT, VER_STR_2_2, "version is set to 2.2 for source project")
        createTestVersion(TARGET_PROJECT , VER_STR_2_2, "target version is set to 2.2")

        //create issue for source project and set affected version
        createIssueAndSetAffectedVersion(SOURCE_PROJECT, VER1_1)
        //create issue for target project and set affected version
        final TARGET_ISSUE = createIssueAndSetAffectedVersion(TARGET_PROJECT, TARGET_VER_1)

        def result = versionService.validateDelete(ctx,VER1_1.id, new SwapVersionAction(SRC_VER2_2.id), new RemoveVersionAction() )

        when: "when source version is deleted"
        if(result.isValid()){
            versionService.delete(ctx, result)
        }

        then: "target version should be deleted"
        Version version = TARGET_PROJECT.versions.find { Version v ->
            v.name == VER_STR1_1
        }

        assert version == null
        assert TARGET_PROJECT.versions.size() == 1
        //expecting affected version set to 2.2
        def targetAffectedVersion = TARGET_ISSUE.affectedVersions.find { it.name == VER_STR_2_2 }
        assert targetAffectedVersion != null
    }

    def "test version merge when target versions available"() {
        setup:
        def eventList = [VersionMergeEvent.name, VersionDeleteEvent.name]
        createTestListener(eventList)

        //creating couple of versions for both testSourceProject and testTargetProject
        final def VER_TO_DELETE = createTestVersion(SOURCE_PROJECT, VER_STR1_1, "version is set to 1.1 for source project")
        final def VER_MERGE_TO = createTestVersion(SOURCE_PROJECT, VER_STR_2_2, "version is set to 2.2 for source project")
        final def TARGET_VER_1 = createTestVersion(TARGET_PROJECT , VER_STR1_1, "target version is set to 1.1")
        createTestVersion(TARGET_PROJECT , VER_STR_2_2, "target version is set to 2.2")

        createIssueAndSetAffectedVersion(SOURCE_PROJECT, VER_TO_DELETE)
        createIssueAndSetAffectedVersion(TARGET_PROJECT, TARGET_VER_1)

        JiraServiceContext ctx = new JiraServiceContextImpl(user)
        def result = versionService.validateMerge(ctx, VER_TO_DELETE.id, VER_MERGE_TO.id,  )

        when: "merge is called in source project merging 1.1 to 2.2"
        if(result.isValid()) {
            versionService.merge(ctx, result)
        }

        then: "in target project same merging should happen"
        assert TARGET_PROJECT.versions.size() == 1
        assert TARGET_PROJECT.versions.find { it.name == VER_STR_2_2 }
    }

    def "test version merge when target versions not available"() {
        setup:
        def eventList = [VersionMergeEvent.name, VersionDeleteEvent.name]
        createTestListener(eventList)

        //creating couple of versions for both testSourceProject and testTargetProject
        final def VER_TO_DELETE = createTestVersion(SOURCE_PROJECT, VER_STR1_1, "version is set to 1.1 for source project")
        final def VER_MERGE_TO = createTestVersion(SOURCE_PROJECT, VER_STR_2_2, "version is set to 2.2 for source project")
        createTestVersion(TARGET_PROJECT , VER_STR1_1, "target version is set to 1.1")
        createTestVersion(TARGET_PROJECT , VER_STR_3_3, "target version is set to 3.3")

        def result = versionService.validateMerge(ctx, VER_TO_DELETE.id, VER_MERGE_TO.id )

        when: "merge is called in source project merging 1.1 to 2.2"
        if(result.isValid()) {
            versionService.merge(ctx, result)
        }

        then: "no merge should happen in target project and target version should not be deleted"
        assert TARGET_PROJECT.versions.size() == 2
        assert TARGET_PROJECT.versions.find { it.name == VER_STR1_1 }
        assert TARGET_PROJECT.versions.find { it.name == VER_STR_3_3 }
    }

    def "test update version"() {
        setup:
        def eventList = [VersionUpdatedEvent.name]
        createTestListener(eventList)

        //creating version for testSourceProject and testTargetProject
        final def SRC_VER1_1  = createTestVersion(SOURCE_PROJECT, VER_STR1_1, "version is set to 1.1 for source project")
        final def TAR_VER_1_1 = createTestVersion(TARGET_PROJECT , VER_STR1_1, "target version is set to 1.1")

        //updating properties for source version
        def versionBuilder =  new VersionBuilderImpl(SRC_VER1_1)
        def UPDATED_NAME = VER_STR1_1 + "_UPDATED"
        def UPDATED_DESCRIPTION = "Test updated description"

        versionBuilder.name(UPDATED_NAME)
        versionBuilder.description(UPDATED_DESCRIPTION)
        def result = versionService.validateUpdate(user, versionBuilder)

        when: "version update is triggered"
        if(result.isValid()) {
            versionService.update(user, result)
        }

        then: "target version name and description should be updated"
        VersionService.VersionResult versionResult = versionService.getVersionById(user, TAR_VER_1_1.id)
        assert versionResult?.version.name == UPDATED_NAME
        assert versionResult?.version.description == UPDATED_DESCRIPTION
    }

    /**
     * Deleting existing test project
     * @param projectKey
     * @return
     */
    private void deleteExistingProject(projectKey) {
        def project = projectManager.getProjectObjByKey(projectKey)
        if (project) {
            def deleteProjectValidationResult = projectService.validateDeleteProject(user, projectKey)
            projectService.deleteProject(user, deleteProjectValidationResult)
        }
    }

    /**
     * Create listener for the test
     * @param eventList
     * @return
     */
    private void createTestListener(eventList) {
        def targetId = projectManager.getProjectObjByKey(TEST_TARGET_PROJECT_KEY).id

        List<Map> listenerConfig = [
                [
                        "id"                                         : "0",
                        (ListenerManagerImpl.FIELD_LISTENER_NOTES)   : "listen for version synchronise test",
                        (ListenerManagerImpl.FIELD_LISTENER_PROJECTS): [TEST_SOURCE_PROJECT_KEY],
                        "canned-script"                              : VersionSynchroniseListener.name,
                        (ListenerManagerImpl.FIELD_LISTENER_EVENTS)  : eventList,
                        "FIELD_TARGET_PROJECT"                       : [targetId]
                ]
        ]
        OSPropertyPersister.save(listenerConfig, ListenerManagerImpl.CONFIG_LISTENERS)
        listenerManager.refresh()
    }

    /**
     * Create issue for a project and set affected version
     * @param project
     * @param affectedVersion
     * @return
     */
    private Issue createIssueAndSetAffectedVersion(Project project, Version affectedVersion) {

        IssueService issueService = ComponentAccessor.issueService
        IssueInputParameters issueInputParameters = issueService.newIssueInputParameters();
        def bugIssueType = getBugIssueType()

        issueInputParameters.setProjectId(project.id)
                .setIssueTypeId(bugIssueType.id)
                .setReporterId(user.name)
                .setSummary("test issue")
                .setStatusId("2")
                .setAffectedVersionIds(affectedVersion.getId())

        IssueService.CreateValidationResult createValidationResult = issueService.validateCreate(user, issueInputParameters);
        Issue issue
        if (createValidationResult.isValid()) {
            IssueService.IssueResult createResult = issueService.create(user, createValidationResult);
            if (createResult.isValid()) {
                issue = createResult.issue
            }

        }
        return issue
    }

    /**
     * Create version for testing
     * @param test_project
     * @param test_version_str
     * @param description
     * @return
     */
    private Version createTestVersion(Project test_project,  String test_version_str, String description){
        VersionService.CreateVersionValidationResult result = versionService.validateCreateVersion(user, test_project, test_version_str, new Date(),description, null)
        if(result.isValid()){
            versionService.createVersion(user, result)
        }
        else
        {
            log.error "Could not create version due to error in validatoin."
        }

    }
}