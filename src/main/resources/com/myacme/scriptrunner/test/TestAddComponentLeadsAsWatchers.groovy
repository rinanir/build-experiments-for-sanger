package com.acme.scriptrunner.test

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueInputParametersImpl
import com.atlassian.jira.project.AssigneeTypes
import com.onresolve.jira.groovy.GroovyFunctionPlugin
import com.onresolve.scriptrunner.canned.jira.utils.CustomScriptDelegate
import com.onresolve.scriptrunner.canned.jira.workflow.postfunctions.CustomScriptFunction
import com.onresolve.scriptrunner.test.AbstractWorkflowSpecification
import com.opensymphony.workflow.loader.ActionDescriptor
import com.opensymphony.workflow.loader.DescriptorFactory
import spock.lang.Shared

class TestAddComponentLeadsAsWatchers extends AbstractWorkflowSpecification {

    @Shared public static final String JOE_LEAD = "joelead"
    @Shared def projectComponentManager = ComponentAccessor.getProjectComponentManager()
    @Shared def currentUser = ComponentAccessor.getJiraAuthenticationContext().getUser()
    @Shared def watcherManager = ComponentAccessor.getWatcherManager()

    /**
     * Create the workflow
     * add the post-function at the Start Progress step
     * Add some components, some with leads, some without
     */
    def setupSpec() {

        def draftWorkflow = getTestDraftWorkflow()

        ActionDescriptor action = draftWorkflow.getAllActions().find {ActionDescriptor ad -> ad.name == "Start Progress"} as ActionDescriptor

        // add post-function script
        def postfunction = DescriptorFactory.getFactory().createFunctionDescriptor()
        postfunction.setType("class")
        postfunction.getArgs().putAll([
            "canned-script": CustomScriptFunction.class.name,
            (CustomScriptDelegate.FIELD_NOTES): "Add component leads as watchers",
            (CustomScriptDelegate.FIELD_SCRIPT_FILE): "com/acme/scriptrunner/scripts/AddComponentLeadsAsWatchers.groovy",
            (CustomScriptDelegate.FIELD_INLINE_SCRIPT): null, // use either a script file or an inline script
            "class.name": GroovyFunctionPlugin.class.name,
        ])

        action.getUnconditionalResult().getPostFunctions().add(0, postfunction)

        publishDraftWorkflow(draftWorkflow)

        def userUtil = ComponentAccessor.getUserUtil()

        // create a user to be a component lead
        if (! userUtil.getUserByName(JOE_LEAD)) {
            userUtil.createUserNoNotification(JOE_LEAD, "xyz", "joelead.example.com", "Joe Lead")
        }

        // create a couple of components for this project
        projectComponentManager.create("Comp1", "first component, has an assignee", JOE_LEAD, AssigneeTypes.PROJECT_DEFAULT, getTestProject().id)
        projectComponentManager.create("Comp2", "second component, no assignee", null, AssigneeTypes.PROJECT_DEFAULT, getTestProject().id)
    }

    def "test script with workflow transitions"() {
        setup:

        def project = getTestProject()

        // create an issue to put through the workflow
        def inputParameters = new IssueInputParametersImpl()

        // note these are the mimimum of fields you need to create an issue, pretty much
        inputParameters.with {
            setProjectId(project.id)
            setIssueTypeId(getBugIssueType().id)
            setReporterId(user.name)
            setSummary("my summary")
            setComponentIds(componentNames.collect {String compname -> projectComponentManager.findByComponentName(project.id, compname).id} as Long[])
            setAssigneeId(user.name)
        }

        when:
        def createValidationResult = issueService.validateCreate(cwdUser, inputParameters)

        then:
        createValidationResult.isValid()

        when:
        def issue = issueService.create(cwdUser, createValidationResult).issue

        then:
        issue.isCreated()

        when:
        // ID: 4 is Start Progress, although we could have found it by name
        def transitionValidationResult = issueService.validateTransition(cwdUser, issue.id, 4, new IssueInputParametersImpl())
        issueService.transition(cwdUser, transitionValidationResult)

        then:
        // do a comparison as a Set because we don't care about the order of the watcher names
        watcherManager.getCurrentWatcherUsernames(issue) as Set == expectedWatchers as Set

        // note the current user is always expected to be a watcher, as it's jira's behaviour
        // (provided you haven't turned off autowatch for the user running the test)
        // a useful improvement to this would be to set it

        where:
        componentNames      | expectedWatchers
        []                  | [currentUser.name]
        ["Comp1"]           | [currentUser.name, JOE_LEAD]
        ["Comp1", "Comp2"]  | [currentUser.name, JOE_LEAD]
    }
}
