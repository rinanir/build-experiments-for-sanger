package com.radbee.scriptrunner
import groovyx.net.http.HTTPBuilder
import static groovyx.net.http.ContentType.TEXT

// shows usage of the http-builder library.
// returns a string of get return code (like: response status:HTTP/1.1 200 OK )


def http = new HTTPBuilder('http://www.google.com')

String response=''
http.get( path : '/search',
        contentType : TEXT,
        query : [q:'Groovy'] ) { resp, reader ->

    response += "EXTRA NEW response status:"+resp.statusLine

}

return response