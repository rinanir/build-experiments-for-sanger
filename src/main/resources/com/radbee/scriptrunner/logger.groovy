package com.radbee.scriptrunner

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import ch.qos.logback.classic.LoggerContext
import ch.qos.logback.core.util.StatusPrinter


        Logger logger = LoggerFactory.getLogger(getClass())
        logger.debug("Hello world.")

        // print internal state
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory()
        StatusPrinter.print(lc)
